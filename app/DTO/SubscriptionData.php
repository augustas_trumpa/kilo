<?php

namespace App\DTO;

class SubscriptionData
{
    /**
     * @var string
     */
    private $subscription_id;

    /**
     * @var string
     */
    private $event_type;

    /**
     * SubscriptionData constructor.
     * @param string $subscription_id
     * @param string $event_type
     */
    public function __construct(string $subscription_id, string $event_type)
    {
        $this->subscription_id = $subscription_id;
        $this->event_type = $event_type;
    }

    /**
     * @return string
     */
    public function getSubscriptionId(): string
    {
        return $this->subscription_id;
    }

    /**
     * @return string
     */
    public function getEventType(): string
    {
        return $this->event_type;
    }

}
