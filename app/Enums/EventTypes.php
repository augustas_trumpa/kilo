<?php

namespace App\Enums;

abstract class EventTypes
{
    const SUBSCRIBTION_RENEWED = 'subscription_renewed';
    const SUBSCRIPTION_FAILED_TO_RENEW = 'subscription_failed_to_renew';
    const SUBSCRIBTION_CANCELED = 'subscription_canceled';
}
