<?php

namespace App\Http\Controllers;

use App\DTO\SubscriptionData;
use App\Enums\EventTypes;
use App\Jobs\SubscriptionJob;
use Symfony\Component\HttpFoundation\JsonResponse;
use Illuminate\Http\Request;

class AppleSubscriptionController extends Controller implements SubscriptionControllerInterface
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request): JsonResponse
    {
        $validated = $this->validate($request, [
            'auto_renew_adam_id' => 'required|string',
            'notification_type' => 'required|string'
        ]);

        dispatch(new SubscriptionJob($this->parseRequest($validated)));

        return new JsonResponse('', 200);
    }

    /**
     * @param array $request
     * @return SubscriptionData
     */
    public function parseRequest(array $request): SubscriptionData
    {
        return new SubscriptionData($request['auto_renew_adam_id'], $this->parseEventType($request['notification_type']));
    }


    /**
     * @param string $type
     * @return EventTypes
     */
   public function parseEventType(string $type): ?string
    {
        switch ($type) {
            case 'CANCEL':
                return EventTypes::SUBSCRIBTION_CANCELED;
            case 'DID_FAIL_TO_RENEW':
                return EventTypes::SUBSCRIPTION_FAILED_TO_RENEW;
            case 'RENEWAL':
                return EventTypes::SUBSCRIBTION_RENEWED;
            default:
                return null;
        }
    }
}
