<?php


namespace App\Http\Controllers;

use App\DTO\SubscriptionData;
use Symfony\Component\HttpFoundation\JsonResponse;
use Illuminate\Http\Request;

interface SubscriptionControllerInterface
{
    function index(Request $request): JsonResponse;
    function parseRequest(array $request): SubscriptionData;
    function parseEventType(string $type): ?string;
}
