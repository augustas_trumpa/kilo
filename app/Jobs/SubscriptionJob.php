<?php

namespace App\Jobs;

use App\DTO\SubscriptionData;
use App\Services\SubscriptionService\SubscriptionService;

class SubscriptionJob extends Job
{
    /**
     * @var SubscriptionData $data
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @param SubscriptionData $data
     */
    public function __construct(SubscriptionData $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $service = new SubscriptionService($this->data);
        $service->execute();
    }
}
