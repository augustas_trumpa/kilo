<?php


namespace App\Services\SubscriptionService;

use App\DTO\SubscriptionData;
use App\Enums\EventTypes;
use App\Subscription;

class SubscriptionService
{
    /**
     * @var SubscriptionData $data
     */
    protected $data;

    /**
     * @var Subscription $subscription
     */
    private $subscription;

    public function __construct(SubscriptionData $data)
    {
        $this->data = $data;
    }

    public function execute(): bool
    {
        $this->subscription = Subscription::where('subscription_id', $this->data->getSubscriptionId())->first();

        if (!$this->subscription) {
            return false;
        }


        switch ($this->data->getEventType()) {
            case EventTypes::SUBSCRIBTION_RENEWED:
                $success = $this->processRenewal();
                break;
            case EventTypes::SUBSCRIPTION_FAILED_TO_RENEW:
                $success = $this->processFailToRenew();
                break;
            case EventTypes::SUBSCRIBTION_CANCELED:
                $success = $this->processCancellation();
                break;
            default:
                $success = false;
                break;
        }

        # todo: insert history record

        return $success;
    }

    private function processCancellation(): bool
    {
        $this->subscription->setAttribute('subscription_status', EventTypes::SUBSCRIBTION_CANCELED);
        return $this->subscription->update();
    }

    private function processRenewal()
    {
        $this->subscription->setAttribute('subscription_status', EventTypes::SUBSCRIBTION_RENEWED);
        return $this->subscription->update();
    }

    private function processFailToRenew()
    {
        $this->subscription->setAttribute('subscription_status', EventTypes::SUBSCRIPTION_FAILED_TO_RENEW);
        return $this->subscription->update();
    }
}
